/*
 * Copyright 2018 Anders Lingfors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef ADDTOSETS_H
#define ADDTOSETS_H

#include "Set.h"
#include "AddToSet.h"
#include "Remove.h"

template <typename T, typename Set> struct AddToSets;

template <typename T, typename FirstSet>
struct AddToSets<T, Set<FirstSet>>
{
    using type = Set<typename AddToSet<T, typename Remove<T, FirstSet>::type>::type>;
};

template <typename T, typename FirstSet, typename... RestSets>
struct AddToSets<T, Set<FirstSet, RestSets...>>
{
    using type = typename AddToSet<typename AddToSet<T, typename Remove<T, FirstSet>::type>::type, typename AddToSets<T, Set<RestSets...>>::type>::type;
};

#endif // ADDTOSETS_H
