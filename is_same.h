/*
 * Copyright 2018 Anders Lingfors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef IS_SAME_H
#define IS_SAME_H

#include "Set.h"
#include "Contains.h"
#include "Remove.h"

template <typename Lhs, typename Rhs>
struct is_same
{
    static const bool value = false;
};

template <>
struct is_same<Set<>, Set<>>
{
    static const bool value = true;
};

template <typename... Ts>
struct is_same<Set<Ts...>, Set<>>
{
    static const bool value = false;
};

template <typename... Ts>
struct is_same<Set<>, Set<Ts...>>
{
    static const bool value = false;
};

template <typename T, typename OtherSet, typename... Ts>
struct is_same<Set<T, Ts...>, OtherSet>
{
    static const bool value = Contains<T, OtherSet>::value && is_same<Set<Ts...>, typename Remove<T, OtherSet>::type>::value;
};

template <typename Lhs, typename Rhs>
constexpr bool is_same_v = is_same<Lhs, Rhs>::value;

#endif // IS_SAME_H
