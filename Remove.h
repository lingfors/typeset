/*
 * Copyright 2018 Anders Lingfors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef REMOVE_H
#define REMOVE_H

#include "Set.h"
#include "AddToSet.h"

template <typename... Ts>
struct Remove;

template <typename T>
struct Remove<T, Set<>>
{
    using type = Set<>;
};

template <typename T, typename U, typename... Us>
struct Remove<T, Set<U, Us...>>
{
    using type = typename AddToSet<U, typename Remove<T, Set<Us...>>::type>::type;
};

template <typename T, typename... Ts>
struct Remove<T, Set<T, Ts...>>
{
    using type = typename Remove<T, Set<Ts...>>::type;
};

#endif // REMOVE_H
